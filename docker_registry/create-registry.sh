#!/usr/bin/env bash
certs=/etc/docker/certs.d/192.168.56.10:8443
mkdir /registry-image
mkdir /etc/docker/certs
mkdir -p $certs
# HTTPS 접속시 서버의 정보가 담긴 인증서와 주고 받는 데이터를 암복호화할 때 사용하는 키가 필요한데, 인증서를 생성하는 요청서가 담긴
# tls.csr 파일로 HTTPS 인증서인 tls.crt 파일과 암호화와 복호화에 사용하는 키인 tls.key 파일을 생성
openssl req -x509 -config $(dirname "$0")/tls.csr -nodes -newkey rsa:4096 \
-keyout tls.key -out tls.crt -days 365 -extensions v3_req

# ssh 접속을 위한 비밀번호를 자동으로 입력하는 sshpass 설치
yum install sshpass -y
for i in {1..3}
  do
    sshpass -p vagrant ssh -o StrictHostKeyChecking=no root@192.168.56.10$i mkdir -p $certs
    sshpass -p vagrant scp tls.crt 192.168.56.10$i:$certs
  done

cp tls.crt $certs
mv tls.* /etc/docker/certs

docker run -d \
  --restart=always \
  --name registry \
  -v /etc/docker/certs:/docker-in-certs:ro \
  -v /registry-image:/var/lib/registry \
  -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/docker-in-certs/tls.crt \
  -e REGISTRY_HTTP_TLS_KEY=/docker-in-certs/tls.key \
  -p 8443:443 \
  registry:2