#!/usr/bin/env bash

# kubeadm을 이용해 k8s 마스터 노드에 접속.
kubeadm join --token 123456.1234567890123456 \
             --discovery-token-unsafe-skip-ca-verification 192.168.56.10:6443 # 인증 무시 + 192.168.56.10:6443 포트에 접속하도록 설정