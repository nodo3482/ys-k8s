#!/usr/bin/env bash

# vim configuration
echo 'alias vi=vim' >> /etc/profile # vi호출시 vim 호출

# 쿠버네티스의 설치 요구 조건을 맞추기 위해 스왑되지 않도록함
swapoff -a
# 시스템이 다시 시작되더라도 스왑되지 않도록 설정
sed -i.bak -r 's/(.+ swap .+)/#\1/' /etc/fstab

# kubernetes repo
gg_pkg="packages.cloud.google.com/yum/doc"
# 쿠버네티스를 내려받을 리퍼지터리 설정
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://${gg_pkg}/yum-key.gpg https://${gg_pkg}/rpm-package-key.gpg
EOF

# docker repo
yum install yum-utils -y
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# selinux가 제한적으로 사용되지 않도록 permissive 모드로 변경
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

# 브리지 네트워크를 통과하는 IPv4와 IPv6의 패킷을 iptables가 관리하게 설정. 파드의 통신을 iptables로 제어한다. 필요에 따라 IPVS 같은 방식으로도 구성할 수 있다.
cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
modprobe br_netfilter # br_netfilter 커널 모듈을 사용해 브리지로 네트워크를 구성한다. IP 마스커레이드(Masquerade)를 사용해 내부 네트워크와 외부 네트워크를 분리.
# IP 마스커레이드: 커널에서 제공하는 NAT 기능

# 쿠버네티스 안에서 노드 간 통신을 이름으로 할 수 있도록 각 노드의 호스트 이름와 IP를 /etc/hosts에 설정. 워커 노드는 Vagrantfile에서 넘겨받은 N변수로 전달된 노스 수에 맞게 동적으로 생성한다.
echo "192.168.56.10 m-k8s-ys" >> /etc/hosts
for (( i=1; i<=$1; i++ )); do echo "192.168.56.10$i w$i-k8s-ys" >> /etc/hosts; done

# 외부와 통신할 수 있게 DNS 서버를 지정
cat <<EOF > /etc/resolv.conf
nameserver 1.1.1.1 #cloudflare DNS
nameserver 8.8.8.8 #Google DNS
EOF