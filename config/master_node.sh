#!/usr/bin/env bash

# kubeadm을 통해 쿠버네티스의 워커 노드를 받아들일 준비. 토큰지정후 토큰 유지 계속되도록함.
kubeadm init --token 123456.1234567890123456 --token-ttl 0 \
--pod-network-cidr=172.16.0.0/16 --apiserver-advertise-address=192.168.56.10 # 워커 노드들이 자동으로 API 서버에 연결되게 한다.

# 마스터노드에서 현재 사용자가 k8s를 정상적으로 구동할 수 있게 설정 파일을 루트의 홈디렉터리에 복사하고 k8s를 이용할 사용자에게 권한을 부여함
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config

# raw_address for gitcontent
raw_git="raw.githubusercontent.com/sysnet4admin/IaC/master/manifests"

# config for kubernetes's network
kubectl apply -f https://$raw_git/172.16_net_calico.yaml

# install bash-completion for kubectl
yum install bash-completion -y

# kubectl completion on bash-completion dir
kubectl completion bash >/etc/bash_completion.d/kubectl

# alias kubectl to k
echo 'alias k=kubectl' >> ~/.bashrc
echo 'complete -F __start_kubectl k' >> ~/.bashrc